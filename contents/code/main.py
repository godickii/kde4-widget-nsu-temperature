# -*- coding: utf-8 -*-
# <Copyright and license information goes here.>
import re, time, pdb
from PyQt4.QtCore import Qt, QTimer, SIGNAL
from PyKDE4.plasma import Plasma
from PyKDE4 import plasmascript
from PyKDE4.kio import KIO
from PyKDE4.kdecore import KUrl

import PyQt4.QtGui 
#TODO: настройки

#pdb.set_trace()

class CurrentTempApplet(plasmascript.Applet):
	def __init__(self,parent,args=None):
		plasmascript.Applet.__init__(self,parent)
		self.URL = 'http://chronos.nsu.ru/tv/graph/loadata.php?tick=2'

	def init(self):
		self.setHasConfigurationInterface(False)

		# Таймер для обновления температуры
		self.resize(125, 125)
		self.setAspectRatioMode(Plasma.Square)

		self.timer = QTimer()
		self.connect(self.timer, SIGNAL("timeout()"), self.loadTemp)
		self.timer.start(1000 * 1800) # Время указывается в миллисекундах

		self.layout = PyQt4.QtGui.QGraphicsLinearLayout(Qt.Horizontal, self.applet)
		self.label = Plasma.Label(self.applet)
		self.label.setText("Loading...")
		print self.layout
		self.layout.addItem(self.label)
		self.applet.setLayout(self.layout)

		# TODO: если поставить здесь 0 или слишком маленькое значение,
		# то при задержке в loadTemp оно не отрисовывается
		QTimer.singleShot(1000, self.loadTemp)

	def loadTemp(self):
		self.job = KIO.storedGet(KUrl(self.URL), KIO.Reload, KIO.HideProgressInfo)
		self.job.addMetaData("User-Agent", "User-Agent: KDE Plasma temperature widget, https://bitbucket.org/godickii/kde4-widget-nsu-temperature")
		self.connect(self.job, SIGNAL("result(KJob*)"), self._get_result)

#	def paintInterface(self, painter, option, rect):
#		painter.save()
#		painter.setPen(Qt.white)
#		painter.drawText(rect, Qt.AlignVCenter | Qt.AlignHCenter, "Hello Python (3)!")
#		painter.drawText(rect, Qt.AlignVCenter | Qt.AlignHCenter, "as")
#		painter.restore()

	def _get_result(self, job):
		content = str(job.data())
		r = re.findall(r"if\(cnv\) cnv.innerHTML = '" + "(.*?)" + r"&deg;C';", content.replace('\n',' '))
		current_temp = r[0] # -23.2 (str)
		self.label.setText((current_temp.split('.')[0]).decode('utf8'))
		#self.label.setText((current_temp + "°C").decode('utf8'))

def CreateApplet(parent):
	return CurrentTempApplet(parent)

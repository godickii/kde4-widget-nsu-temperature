Репозиторий: https://bitbucket.org/godickii/kde4-widget-nsu-temperature

Отладочный просмотр:
	plasmoidviewer

Уствовка:
	zip -r /tmp/kde4-widget-nsu-temperature.zip .
	plasmapkg -i /tmp/kde4-widget-nsu-temperature.zip
Удаление:
	plasmapkg -r nsu-temp

Tutorial:
	Hello world applet: http://techbase.kde.org/Development/Tutorials/Plasma/Python/Using_widgets

Работа с таймером в pyqt4:
	http://www.rkblog.rk.edu.pl/w/p/qtimer-making-timers-pyqt4/

Требуется пакет:
	plasma-scriptengine-python
